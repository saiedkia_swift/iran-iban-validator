//
//  GetNameTest.swift
//  ShebaValidator_Tests
//
//  Created by salar on 8/17/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import XCTest
import ShebaValidator

class GetNameTest: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_getName_1() {
        let validator = ShebaValidator()
        let sheba = "IR070190000008388383883838"
        let expectedBankName = "صادرات"
        let result = validator.getName(sheba: sheba)
        
        XCTAssert(expectedBankName == result)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
