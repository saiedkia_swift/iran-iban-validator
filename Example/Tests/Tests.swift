import XCTest
@testable import ShebaValidator

class Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_Example_A() {
        let validator = ShebaValidator()
        let code = validator.getCharCode(char: "A")
        let expected = 10
        
        
        XCTAssert(code == expected)
    }
    
    func test_Example_Z() {
        let validator = ShebaValidator()
        let code = validator.getCharCode(char: "Z")
        let expected = 35
        
        
        XCTAssert(code == expected)
    }
    
    
    func test_validate_code() {
        let sheba = "IR062960000000100324200001"
        
        let result = ShebaValidator().validate(sheba: sheba)
        
        XCTAssertTrue(result, "sheba code validation")
    }
    
    func test_validate_should_fail(){
        let sheba = "IZ062960000000100324200001"
        
        let result = ShebaValidator().validate(sheba: sheba)
        
        XCTAssertFalse(result, "sheba code validation")
    }
    
    func test_validate_should_fail_(){
        let sheba = "IZ06296000000010032420000"
        
        let result = ShebaValidator().validate(sheba: sheba)
        
        XCTAssertFalse(result, "sheba code validation")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure() {
            // Put the code you want to measure the time of here.
        }
    }
    
}
