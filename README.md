# ShebaValidator

[![CI Status](https://img.shields.io/travis/saiedkia/ShebaValidator.svg?style=flat)](https://travis-ci.org/saiedkia/ShebaValidator)
[![Version](https://img.shields.io/cocoapods/v/ShebaValidator.svg?style=flat)](https://cocoapods.org/pods/ShebaValidator)
[![License](https://img.shields.io/cocoapods/l/ShebaValidator.svg?style=flat)](https://cocoapods.org/pods/ShebaValidator)
[![Platform](https://img.shields.io/cocoapods/p/ShebaValidator.svg?style=flat)](https://cocoapods.org/pods/ShebaValidator)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ShebaValidator is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ShebaValidator'
```
### Usage
```swift
let shebaValidator = ShebaValidator()
let isValid = shebaValidator.validate(sheba: "IR030083838372536483625475")
let bankName = shebaValidator.getName(sheba: "IR030083838372536483625475")
```
## Author

saiedkia

## License

ShebaValidator is available under the MIT license. See the LICENSE file for more info.
