#
# Be sure to run `pod lib lint ShebaValidator.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ShebaValidator'
  s.version          = '0.0.1'
  s.summary          = 'Sheba IBAN code validator'
  s.homepage         = 'https://gitlab.com/saiedkia_swift/iran-iban-validator'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'saiedkia' => 'saiedkia.dev@outlook.com' }
  s.source           = { :git => 'https://gitlab.com/saiedkia_swift/iran-iban-validator.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'ShebaValidator/Classes/**/*'
  
  # s.resource_bundles = {
  #   'ShebaValidator' => ['ShebaValidator/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'BigInt', '~> 4.0'
end
