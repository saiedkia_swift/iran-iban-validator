//
//  Banks.swift
//  FBSnapshotTestCase
//
//  Created by salar on 8/13/19.
//

import Foundation


public enum Banks:Int {
    case eghtesadNovin = 55
    case parsian = 54
    case pasargad = 57
    case postBank = 21
    case tejarat = 18
    case toseEtebari = 51
    case toseSaderat = 20
    case refah = 13
    case saman = 56
    case sepah = 15
    case sarmaye = 58
    case saderat = 19
    case sanatOMadan = 11
    case karAfarin = 53
    case keshavarzi = 16
    case markazi = 10
    case maskan = 14
    case mellat = 12
    case melli = 17
    
    
    func getName() -> String {
        switch self {
        case .saderat:
            return "صادرات"
        case .karAfarin:
            return "کار آفرین"
        case .sepah:
            return "سپه"
        case .refah:
            return "رفاه کارگران"
        case .keshavarzi:
            return "کشاورزی"
        case .melli:
            return "ملی"
        case .mellat:
            return "ملت"
        case .saman:
            return "سامان"
        case .parsian:
            return "پارسیان"
        case .tejarat:
            return "تجارت"
        case .eghtesadNovin:
            return "اقتصاد نوین"
        case .postBank:
            return "پست بانک"
        case .sanatOMadan:
            return "صنعت و معدن"
        case .maskan:
            return "مسکن"
        case .toseEtebari:
            return "توسعه اعتباری"
        case .markazi:
            return "مرکزی"
        case .pasargad:
            return "پاسارگاد"
        case .sarmaye:
            return "سرمایه"
        case .toseSaderat:
            return "توسعه صادرات"
        }
    }
}



