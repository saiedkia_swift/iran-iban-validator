import BigInt

public class ShebaValidator {
    
    public init() {
        
    }
    
    public func validate(sheba:String?) -> Bool {
        if sheba?.count != 26 {
            return false
        }
        
        if sheba?.first != Character("I") && sheba?.dropFirst().first != Character("R") {
            return false
        }
        
        let charOne = getCharCode(char: Character(sheba!.substr(fromIndex: 0, toIndex: 1)!))!
        let charTwo = getCharCode(char: Character(sheba!.substr(fromIndex: 1, toIndex: 2)!))!
        
        let rSheba = "\(sheba!.substr(fromIndex: 4, toIndex: 26)!)\(charOne)\(charTwo)\(sheba!.substr(fromIndex: 2, toIndex: 4)!)"

        if let bInt = BigInt(rSheba) {
            if bInt % 97 == 1 { return true}
        }
        
        return false
    }
    
    
    public func getCharCode(char:Character) -> Int? {
        var currentChar = Character("A")
        
        var value = 10
        for _ in 0..<32 {
            
            if currentChar == char {
                return value
            }
            
            currentChar = currentChar.next()
            value += 1
        }
        
        
        return nil
    }
    
    public func getName(sheba:String?) -> String? {
        if validate(sheba: sheba),  let strCode =  sheba!.substr(fromIndex: 5, toIndex: 7) {
            if let intCode = Int(strCode) {
                if let bankId = Banks(rawValue: intCode) {
                    return bankId.getName()
                }
            }
        }
        
        return nil
    }
}


extension Character {
    func next() -> Character {
        let code = self.asciiValue!
        if code >= 65 && code <= 90 {
            let unicode = UnicodeScalar(code + 1)
            return Character(unicode)
        }
        
        return self
    }
}

extension String {
    func substr(fromIndex: Int, toIndex: Int) -> String? {
        if fromIndex < toIndex && toIndex <= self.count {
            let startIndex = self.index(self.startIndex, offsetBy: fromIndex)
            let endIndex = self.index(self.startIndex, offsetBy: toIndex)
            return String(self[startIndex..<endIndex])
        }else{
            return nil
        }
    }
}
