//
//  Extensions.swift
//  FBSnapshotTestCase
//
//  Created by salar on 8/14/19.
//

import Foundation


extension String {
     func substr(fromIndex: Int, toIndex: Int) -> String? {
        if fromIndex < toIndex && toIndex <= self.count {
            let startIndex = self.index(self.startIndex, offsetBy: fromIndex)
            let endIndex = self.index(self.startIndex, offsetBy: toIndex)
            return String(self[startIndex..<endIndex])
        }else{
            return nil
        }
    }
}
